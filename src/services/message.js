const messageModel = require("../dao/models/message");

module.exports = class {
  async addMessage(message) {
    const messageCreated = await messageModel.create(message);
    return messageCreated;
  }
  async getAllMessages() {
    const messages = await messageModel.find();
    return messages;
  }
};
