const {
  listProducts,
  createProduct,
  getProduct,
  updateProduct,
  deleteProduct,
} = require("../controllers/product");
module.exports = (router) => {
  router.get("/api/productos", listProducts);
  router.get("/api/productos/:id", getProduct);
  router.post("/api/productos", createProduct);
  router.put("/api/productos/:id", updateProduct);
  router.delete("/api/productos/:id", deleteProduct);
  return router;
};
