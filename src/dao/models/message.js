const { Schema, model } = require("mongoose");

const messageSchema = new Schema({
  email: String,
  date: String,
  message: String,
});

module.exports = model("Message", messageSchema);
