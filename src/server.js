const http = require("http");
const express = require("express");
const app = express();
const server = http.createServer(app);
const router = express.Router();

const { Server } = require("socket.io");
const io = new Server(server);

const moment = require("moment");

const { PORT } = require("./config/globals");

const productRoutes = require("./routes/product");
const { getConnection } = require("./dao/db/connection");

app.set("views", "./src/views");
app.set("view engine", "ejs");

//paso el socket
app.use((req, res, next) => {
  req.io = io;
  next();
});

app.use(express.json());
app.use(express.urlencoded());

app.use("/public", express.static("./src/resources"));
app.get("/ingreso", (req, res) => {
  req.io = io;
  res.render("input");
});

app.use(productRoutes(router));

getConnection().then(() =>
  server.listen(PORT, () => console.log("server's up", PORT))
);

io.on("connection", async (socket) => {
  const ProductService = require("./services/product");
  const MessageService = require("./services/message");
  productService = new ProductService();
  messageService = new MessageService();

  console.log("new connection", socket.id);

  io.sockets.emit("list:products", await productService.getAllProducts());
  io.sockets.emit("chat:messages", await messageService.getAllMessages());

  socket.on("chat:new-message", async (data) => {
    const message = {
      email: data.email,
      date: `[${moment().format("DD/MM/YYYY hh:mm:ss")}]`,
      message: data.message,
    };
    console.log(message);
    await messageService.addMessage(message);
    io.sockets.emit("chat:messages", await messageService.getAllMessages());
  });
});
